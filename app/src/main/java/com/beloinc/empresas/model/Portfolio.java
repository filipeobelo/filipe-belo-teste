package com.beloinc.empresas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Portfolio {

    @SerializedName("enterprises_number")
    @Expose
    private Integer enterprisesNumber;
    @SerializedName("enterprises")
    @Expose
    private List<Object> enterprises = null;

    public Integer getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(Integer enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }

}