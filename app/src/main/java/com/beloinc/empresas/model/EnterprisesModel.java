package com.beloinc.empresas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnterprisesModel {

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprises = null;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

}