package com.beloinc.empresas.service;

import com.beloinc.empresas.model.EnterprisesModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Query;

public interface EnterpriseClient {

    @GET("enterprises")
    Call<EnterprisesModel> getEnterprise(
            @HeaderMap Map<String, String> headers,
            @Query("enterprise_types") String type,
            @Query("name") String enterpriseName
            );

}
