package com.beloinc.empresas.service;



import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
Generic service class with static instances and methods to be reused along the app
 */

public class ServiceGenerator {

    private static final String BASE_URL = "http://54.94.179.135:8090/api/v1/";


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }


}
