package com.beloinc.empresas.service;

import com.beloinc.empresas.model.Login;
import com.beloinc.empresas.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserClient {

    @POST("users/auth/sign_in")
    Call<User> loginUser(@Body Login login);
}
