package com.beloinc.empresas.user_interface;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beloinc.empresas.R;
import com.beloinc.empresas.model.Enterprise;
import com.beloinc.empresas.model.EnterprisesModel;
import com.beloinc.empresas.service.EnterpriseClient;
import com.beloinc.empresas.service.ServiceGenerator;
import com.beloinc.empresas.user_interface.utils.ContainerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    private EditText mEditText;
    private ImageView mSearchIc;
    private TextView mInstruction;

    private RecyclerView mRecyclerView;
    private ContainerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ConstraintLayout mSearchUp;
    private ConstraintLayout mSearchDown;

    private Boolean search = false;
    private List<Enterprise> enterprises;
    private List<String> customHeaders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.d(TAG, "onCreate: HomeActivity started");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        customHeaders = getIntent().getStringArrayListExtra("headers");
        Log.d(TAG, "onCreate: headers: " + customHeaders.get(0) + " | " + customHeaders.get(1) + " | " + customHeaders.get(2));

        setupWidgets();

        searchUp(false);

        setupClickListener(new WidgetsClickListener());

    }

    //start background service API
    private void setupService(List<String> customHeaders, String search) {
        Log.d(TAG, "setupService: starting service");
        EnterpriseClient enterpriseClient = ServiceGenerator.createService(EnterpriseClient.class);
        Map<String, String> map = new HashMap<>();

        map.put("access-token", customHeaders.get(0));
        map.put("client", customHeaders.get(1));
        map.put("uid", customHeaders.get(2));

        if (search != null && !search.equals("")) {
            Call<EnterprisesModel> call = enterpriseClient.getEnterprise(map, null, search);

            call.enqueue(new Callback<EnterprisesModel>() {
                @Override
                public void onResponse(Call<EnterprisesModel> call, Response<EnterprisesModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.d(TAG, "onResponse: success ");
                            enterprises = response.body().getEnterprises();

                            setupRecyclerView();
                        }
                    } else {
                        Log.d(TAG, "onResponse: no such enterprise");
                    }
                }

                @Override
                public void onFailure(Call<EnterprisesModel> call, Throwable t) {
                    Toast.makeText(HomeActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: ");
                }
            });
        }
    }


    private void setupRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        //linear layout
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //adapter
        mAdapter = new ContainerAdapter(enterprises);
        mAdapter.setListener(new ContainerAdapter.Listener() {
            @Override
            public void onClick(Enterprise enterprise) {
                Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                intent.putStringArrayListExtra("enterprise", setupIntent(enterprise));
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private ArrayList<String> setupIntent(Enterprise enterprise) {
        ArrayList<String> detailInfo = new ArrayList<>();
        detailInfo.add(enterprise.getEnterpriseName());
        detailInfo.add(enterprise.getDescription());
        return detailInfo;
    }


    private void setupWidgets() {
        mEditText = findViewById(R.id.search_edit_text);
        mSearchIc = findViewById(R.id.ic_search);
        mSearchUp = findViewById(R.id.search_up);
        mSearchDown = findViewById(R.id.search_down);
        mInstruction = findViewById(R.id.instruction);
        mRecyclerView = findViewById(R.id.recycler_view);
    }

    //handle visibility of search engine
    private void searchUp(boolean up) {
        if (up) {
            search = true;
            mSearchDown.setVisibility(View.GONE);
            mInstruction.setVisibility(View.GONE);
            mSearchUp.setVisibility(View.VISIBLE);
            openKeyboard(true);
            //TEXT WATCHER SO SEARCH BEGINS AUTOMATICALLY WHEN USER TYPE AT EDIT TEXT
            setupTextWatcher();
        } else {
            search = false;
            openKeyboard(false);
            clearAdapter();
            mSearchUp.setVisibility(View.GONE);
            mSearchDown.setVisibility(View.VISIBLE);
            mInstruction.setVisibility(View.VISIBLE);
        }
    }

    /**
     * TEXT WATCHER SO SEARCH BEGINS AUTOMATICALLY WHEN USER TYPE AT EDIT TEXT
     */
    private void setupTextWatcher() {
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                clearAdapter();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    if (!s.toString().equals("")) {
                        setupService(customHeaders, s.toString());
                    }
                } else {
                    clearAdapter();
                }
            }
        });
    }

    //clear recyclerview adapter
    private void clearAdapter() {
        if (enterprises != null) {
            enterprises.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void openKeyboard(Boolean open) {
        if (open) {
            mEditText.setEnabled(true);
            mEditText.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
            }
        } else {
            mEditText.getText().clear();
            mEditText.setEnabled(false);
        }
    }


    //handle clicks at drawable right (close) at edit text
    @SuppressLint("ClickableViewAccessibility")
    private void setupClickListener(WidgetsClickListener widgetsClickListener) {
        mSearchIc.setOnClickListener(widgetsClickListener);

        mEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mEditText.getRight() - mEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Log.d(TAG, "onTouch: close search button clicked");
                        searchUp(false);
                    }
                }

                return false;
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (search) {
            searchUp(false);
        } else {
            super.onBackPressed();
        }
    }

    private class WidgetsClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ic_search:
                    searchUp(true);
                    break;
            }
        }
    }
}
