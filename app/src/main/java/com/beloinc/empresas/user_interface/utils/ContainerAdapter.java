package com.beloinc.empresas.user_interface.utils;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beloinc.empresas.R;
import com.beloinc.empresas.model.Enterprise;
import com.beloinc.empresas.model.EnterpriseType;

import java.util.List;

public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.ViewHolder> {
    private List<Enterprise> enterprisesList;
    private Listener listener;

    public interface Listener {
        public void onClick(Enterprise enterprise);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ContainerAdapter(List<Enterprise> enterprisesList) {
        this.enterprisesList = enterprisesList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout mLayout;

        public ViewHolder(ConstraintLayout itemView) {
            super(itemView);
            mLayout = itemView;
        }
    }


    @NonNull
    @Override
    public ContainerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout mLayout = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.container_adapter, parent, false);
        return new ViewHolder(mLayout);
    }


    @Override
    public void onBindViewHolder(@NonNull ContainerAdapter.ViewHolder holder, int position) {
        ConstraintLayout constraintLayout = holder.mLayout;

        final Enterprise enterprise = enterprisesList.get(position);
        EnterpriseType enterpriseType = enterprise.getEnterpriseType();

        //widgets
        ImageView logoView = constraintLayout.findViewById(R.id.enterprise_logo);
        TextView nameView = constraintLayout.findViewById(R.id.enterprise_name);
        TextView typeView = constraintLayout.findViewById(R.id.enterprise_type);
        TextView countryView = constraintLayout.findViewById(R.id.enterprise_country);

        nameView.setText(enterprise.getEnterpriseName());
        typeView.setText(enterpriseType.getEnterpriseTypeName());
        countryView.setText(enterprise.getCountry());

        //set click listener on each item
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(enterprise);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return enterprisesList.size();
    }


}
