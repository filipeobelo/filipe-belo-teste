package com.beloinc.empresas.user_interface;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.beloinc.empresas.R;

import java.util.ArrayList;

/**
 * NO IMAGE URLS RETURNED FROM API (THEY ARE ALL NULL)
 */

public class DetailActivity extends AppCompatActivity {
    private static final String TAG = "DetailActivity";

    private ImageView logoView;
    private TextView descriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ArrayList<String> enterpriseInfo = getIntent().getStringArrayListExtra("enterprise");

        setupToolbar(enterpriseInfo.get(0));

        setupWidgets();

        setupUi(enterpriseInfo);

    }

    private void setupUi(ArrayList<String> enterpriseInfo) {
        descriptionView.setText(enterpriseInfo.get(1));
    }

    private void setupToolbar(String name) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    private void setupWidgets() {
        logoView = findViewById(R.id.logo_detail);
        descriptionView = findViewById(R.id.description_detail);
    }
}
